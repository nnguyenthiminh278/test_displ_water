import numpy as np
import math
import sys

# -------------------------------------------------------------------------
# ----- This script is used to solve the linear combination ---------------
# -------------- and get the coefficients which are -----------------------
# type: python3 Coef_4.py outfile.txt
# -------------------------------------------------------------------------
cm1_au = 4.5563353e-6      # cm**-1 to au
cm1_au = float(cm1_au)
# open the normal mode coords and save into a list of linear combination vectors
my_list= [] 
with open('nm_ES.txt', 'r') as f_nm:
    for line in f_nm.readlines():
        for num in line.split(' '):
            my_list.append(float(num))
            A = np.array(my_list)
            A.resize(3,9)
            B = A.T
print("Linear combination vectors: %s" % A)
print(np.shape(A))

print("Linear combination vectors transpose: %s" % B)
print(np.shape(B))

# Open a coord file of displacement
y = [] 
with open('displ.txt', 'r') as f_dif:
    for line in f_dif.readlines():
        for num in line.split(' '):
            y.append(float(num))
        
print("Coordinates of displacement: %s" % (y))

#Calculate the coeficients
result = []
#w = [1146,1701,2602]
#h = 1 # planck constant
factor1 = math.sqrt(1146*cm1_au) 
factor2 = math.sqrt(1701*cm1_au) 
factor3 = math.sqrt(2602*cm1_au) 

def savecoord(result, outfile):
    scalars = y@B
    result = np.reshape(scalars,(3, 1))
    result[0,:] *= factor1   
    result[1,:] *= factor2        
    result[2,:] *= factor3            
    np.savetxt(outfile, result, delimiter=" ")
# Close opend file
f_nm.close()
f_dif.close()

if __name__ == "__main__":
    outfile = sys.argv[1]
    savecoord(result, outfile)