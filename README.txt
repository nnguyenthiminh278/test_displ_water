----------------------
 File list
----------------------

-----Inputs------
H2O_GS.fchk		Ground state structure
H2O_ES.fchk		Excited state structure

-----Scripts-----
Get_coord_1.py		To get the coordinates of GS/ES from Gaussian (.fchk file)
Subtract_2.py		To get the difference between the coordinate of GS and ES
Get_nm_3        	To get the normal modes's coordinates
Coef_4          	To get the coefficients of the linear combination

-----Outputs-----
GS.txt  		Coordinate of ground state
ES.txt   		Coordinate of excited state
displ.txt		Displacement of the coordinate of ES with respect to the GS
nm_ES.txt		Normal modes coordinates
outfile.txt		Values of coefficients of linear combination

-----Others-----
README			This file

----------------------
Command Line Arguments
----------------------
python3 Get_coord_1.py H2O_GS.fchk GS.txt 
python3 Get_coord_1.py H2O_ES.fchk ES.txt 

python3 Subtract_2.py displ.txt

python3 Get_nm_3.py H2O_ES.fchk nm_ES.txt

python3 Coef_4.py outfile.txt

----------------------
Some notes
----------------------
I'd like to first mention that this code is made specially for water molecule. Then for general case I will fix it later.

In Get_nm_3.py, there is also some line to test the orthogonalization of the normal mode vectors. Please remove # in case you want to turn on this test.

